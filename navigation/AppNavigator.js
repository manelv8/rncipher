import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../screens/HomeScreen';
import CipherScreen from '../screens/CipherScreen';

const AppNavigator = createStackNavigator({
  Home: HomeScreen,
  Cipher: CipherScreen
})

export default createAppContainer(AppNavigator)
