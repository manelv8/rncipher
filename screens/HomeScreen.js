import React, { useState } from 'react';
import { 
  View, 
  Text,
  TextInput, 
  Button,
  StyleSheet,
  Image,
  Picker,
  KeyboardAvoidingView } from "react-native";

const HomeScreen = props => {
  const [ text0, setText0 ] = useState('')
  const [ text1, setText1 ] = useState('');
  const [ cifraLabel, setCifraLabel ] = useState('');

  return (
    <KeyboardAvoidingView style={styles.screen}>
      <Image
        source={require("../assets/scouts.png")}
        style={{ height: 120, width: 175 }}
      />
      <Text style={{ fontSize: 20, color: "#0d2043" }}>
        Codificador de Mensagens{" "}
      </Text>
      <View style={{ padding: 20 }}>
        <TextInput
          //Texto a cifrar
          style={{
            height: 50,
            width: 250,
            padding: 5,
            backgroundColor: "white",
            fontSize: 15
          }}
          placeholder="Escreva aqui o texto a cifrar..."
          placeholderTextColor="orange"
          value={text0}
          onChangeText={data => setText0(data)}
        />
      </View>

      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          fontSize: 15,
          height: 50,
          width: 250
        }}
      >
        <Picker
          //Escolha da cifra
          selectedValue={cifraLabel}
          style={{
            height: 50,
            width: 250,
            alignItems: "center",
            justifyContent: "center",
            color: "orange"
          }}
          onValueChange={(itemValue, itemIndex) => {
            // this.setState({ cifraLabel: itemValue, cifraIndex: itemIndex })
            console.log([itemValue, itemIndex]);
            setCifraLabel(itemValue);
          }}
        >
          <Picker.Item label="Escolha a cifra..." value="cifras" />
          <Picker.Item label="Alfabeto Invertido" value="AlfaInvert" />
          <Picker.Item label="Alfabeto Numeral" value="AlfaNumeral" />
          <Picker.Item label="Angular" value="Angular" />
          <Picker.Item label="Batalha Naval" value="BatalhaNCerta" />
          <Picker.Item label="Caranguejo" value="Caranguejo" />
          <Picker.Item label="César" value="Cezar" />
          <Picker.Item label="Código Braille (Falso)" value="CBraille" />
          <Picker.Item label="Código +3" value="Código+3" />
          <Picker.Item label="Código Chinês 1" value="Chines1" />
          <Picker.Item label="Código Chinês 2" value="Chines2" />
          <Picker.Item label="Data" value="Data" />
          <Picker.Item label="Frase-Chave-Vertical" value="FraseVertical" />
          <Picker.Item label="Frase-Chave-Horizontal" value="FraseHorizontal" />
          <Picker.Item label="Homógrafo" value="Homografo" />
          <Picker.Item label="Metades" value="Metades" />
          <Picker.Item label="Morse" value="Morse" />
          <Picker.Item label="Nós de Morse" value="NosMorse" />
          <Picker.Item label="Passa um Melro" value="PassaMelro" />
          <Picker.Item label="Passa dois Melros" value="Passa2Melros" />
          <Picker.Item label="Picos de Morse" value="PicosMorse" />
          <Picker.Item label="Primeira Letra Falsa" value="PrimeiraFalsa" />
          <Picker.Item label="Romano-Árabe" value="RomanoArabe" />
          <Picker.Item label="SMS" value="SMS" />
          <Picker.Item label="Transposto" value="Transposto" />
          <Picker.Item label="Última Letra Falsa" value="UltimaFalsa" />
          <Picker.Item label="Vogais por Pontos" value="VogaisPontos" />

          {/* <Picker.Item label="A minha cifra" value="A minha cifra" />
                 label =>valor a mostrar no ecrâ ;
                 value= valor a ir para slideshow;*/}
        </Picker>
      </View>
      <View>
        {cifraLabel == "Data" ||
        cifraLabel == "Transposto" ||
        cifraLabel == "AlfaNumeral" ||
        cifraLabel == "FraseVertical" ||
        cifraLabel == "FraseHorizontal" ||
        cifraLabel == "BatalhaNCerta" ||
        cifraLabel == "Cezar" ? (
          <TextInput
            //Apresenta um campo para colocar password em algumas cifras(ver acima)
            style={{
              height: 50,
              width: 250,
              padding: 5,
              backgroundColor: "white",
              fontSize: 15,
              marginTop: 20
            }}
            placeholder="Password..."
            placeholderTextColor="orange"
            value={text1}
            onChangeText={data => setText1(data)}
            editable={true}
          />
        ) : null}
      </View>
      <View style={{ padding: 10 }} />
      <View>
        <Button
          //Botão que envia os dados introduzidos para outra tab
          title="Cifrar Mensagem"
          color="orange"
          // onPress={this.CheckTextInput}
        />
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
export default HomeScreen;